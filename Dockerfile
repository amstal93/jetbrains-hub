FROM registry.gitlab.com/jitesoft/dockerfiles/openjdk:latest
LABEL maintainer="Johannes Tegnér <johannes@jitesoft.com>"

ARG VERSION="2018.4.11436"
ENV PORT="8080" \
    PUBLIC_URI="http://127.0.0.1:8080"

ADD startup.sh /startup.sh
ADD healthcheck.sh /healthcheck.sh
RUN apk add --no-cache --virtual .trash wget unzip \
 && wget --tries=5 --progress=dot:giga https://download.jetbrains.com/hub/hub-${VERSION}.zip \
 && wget --tries=5 --progress=dot:giga https://download.jetbrains.com/hub/hub-${VERSION}.zip.sha256 \
 && grep "hub-${VERSION}.zip\$" hub-${VERSION}.zip.sha256 | sha256sum -c - \
 && unzip -q hub-${VERSION}.zip \
 && rm hub-${VERSION}.zip hub-${VERSION}.zip.sha256 \
 && mv hub-* /hub \
 && apk del .trash \
 && chmod +x /startup.sh \
 && chmod +x /healthcheck.sh

VOLUME ["/hub/data", "/hub/backups", "/hub/logs", "/hub/conf"]
HEALTHCHECK --interval=2m --timeout=5s CMD /healthcheck.sh
CMD ["/startup.sh"]
